﻿namespace ShowBing.Core.Dto
{

    /// <summary>
    /// 命名规则与LayUI相同。
    /// </summary>
    public class BasePageInput
    { 
        /// <summary>
        /// 页码1开始
        /// </summary>
        public int Page { get; set; } = 1;
        /// <summary>
        /// 页大小
        /// </summary>
        public int Limit { get; set; } = 20;
    } 

}
