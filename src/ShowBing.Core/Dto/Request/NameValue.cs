﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowBing.Core.Dto
{
    public class NameValue<TValueType>
    {
        public TValueType Value { get; set; }
        public string Name { get; set; }
    }

    public class NameValue : NameValue<long>
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class NameValueString : NameValue<string>
    {

    }

    /// <summary>
    /// 
    /// </summary>
    public class NameValueId : NameValueString
    {
        public long Id { get; set; }   
    }

   

}
