﻿namespace ShowBing.Core.Dto.Response
{
    public class BingImageOutput
    {
         
        public long Id { get; set; }
        
        public int ImageDate { get; set; }
 
        public int Width { get; set; } = 1920;
        
        public int Height { get; set; } = 1080;

        /// <summary>
        /// 文件位置 /yyyyMM/yyyyMMdd_1920x1080.jpg
        /// </summary>
        public string Url { get; set; }

        public string Copyright { get; set; }

        public string CopyrightLink { get; set; }
        public string Thumbnail => BingWallpaperHelper.GetThumbnailFileName(Url);
    }

   

}
