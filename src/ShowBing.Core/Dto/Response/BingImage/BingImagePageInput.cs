﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowBing.Core.Dto.Response
{
    public class BingImagePageInput:BasePageInput
    {
        public string Begin { get; set; }
        public string End { get; set; }

        public string Title { get; set; }

    }

   

}
