﻿namespace ShowBing.Core.Dto
{
    public class DataResult<TModel> : SimpleResult
    {
        public TModel Data { get; set; }
    }

}
