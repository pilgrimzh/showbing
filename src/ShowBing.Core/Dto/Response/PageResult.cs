﻿using System.Collections.Generic;

namespace ShowBing.Core.Dto
{
    public class PageResult<TModel> : SimpleResult
    {
        public IEnumerable<TModel> Data { get; set; }
        public long Count { get; set; }
        
    }

}
