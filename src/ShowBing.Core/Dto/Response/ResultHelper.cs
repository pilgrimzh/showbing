﻿using System;
using System.Collections.Generic;

namespace ShowBing.Core.Dto
{
     
    public class ResultHelper
    {
        public static SimpleResult Ok => new SimpleResult { Success = true,Msg="操作成功！" };
        public static SimpleResult OkMessage(string message)
        {
            return new SimpleResult { Success = true, Msg = message };
        }

        public static SimpleResult FailureMessage(string message)
        {
            return new SimpleResult { Success = false, Msg = message };
        }
        public static SimpleResult FailureNotExists(string name="记录" ,string message="不存在！")
        {
            return FailureMessage($"{name}{message}");
        }
        public static SimpleResult FailureExists(string name = "记录", string message = "已存在！")
        {
            return FailureMessage($"{name}{message}");
        }
        public static SimpleResult FailureRequired(string name , string message = "必须填写！")
        {
            return FailureMessage($"{name}{message}");
        }
        public static SimpleResult FailureOutRange(string name , string message = "超出范围值！")
        {
            return FailureMessage($"{name}{message}");
        }
        public static SimpleResult FailureInvalid(string name, string message = "为无效值！")
        {
            return FailureMessage($"{name}{message}");
        }


        public static DataResult<TModel> SuccessDataResult<TModel>(TModel data, string message="")
        {
            return new DataResult<TModel> { Data = data, Success = true, Msg = message };
        }
        public static PageResult<TModel> SuccessPageResult<TModel>(IEnumerable<TModel> data,long total,string message = "")
        {
            return new PageResult<TModel> { Data = data, Count = total, Success = true, Msg = message };
        }


    }

}
