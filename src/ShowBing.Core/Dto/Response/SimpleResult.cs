﻿namespace ShowBing.Core.Dto
{
    public class SimpleResult
    {
        public const int SuccessCode = 0;
        public SimpleResult()
        {

        }

        public int Code { get; set; }
        public bool Success
        {
            get
            {
                return Code == SuccessCode;
            }
            set
            {
                Code = value ? SuccessCode : 500;
            }
        }

        public string Msg { get; set; }
    }

}
