﻿using System;
using System.Collections.Generic;

namespace ShowBing.Core
{
    public static class DateTimeExtensions
    {
        public static string WeekText(this DateTime dateTime)
        {
            Dictionary<DayOfWeek,string> map=new Dictionary<DayOfWeek, string>{
               { DayOfWeek.Sunday,"周日" },
               { DayOfWeek.Monday,"周一" },
               { DayOfWeek.Tuesday,"周二" },
               { DayOfWeek.Wednesday,"周三" },
               { DayOfWeek.Thursday,"周四" },
               { DayOfWeek.Friday,"周五" },
               { DayOfWeek.Saturday,"周六" }
           };
           return map[dateTime.DayOfWeek];
        }
    }

 }
