﻿using System.Collections.Generic;

namespace ShowBing.Core
{
    public static class ObjectExtensions
    {

        /// <summary>
        /// 对象属性初始化，属性为Null的字符串类型=>string.Empty
        /// 初始化为String.Empty,0, DateTime.MinValue, DateTimeOffset.MinValue
        /// </summary> 
        /// <param name="model">对象</param>
        /// <returns></returns>
        public static TModel SetNullStringToEmpty<TModel>(this TModel model) where TModel : class
        {
            var properties = model.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.CanRead || !property.CanWrite) continue;

                var ptype = property.PropertyType;
                if (ptype == typeof(string) && property.GetValue(model) == null)
                {
                    property.SetValue(model, string.Empty);
                }
            }
            return model;
        }

        public static bool IsNull(this object obj)
        {
            return obj == null;
        }
        public static bool IsNotNull(this object obj)
        {
            return obj != null;
        }

        public static string ToSafeString(this object obj)
        {
            return obj.IsNull()==true ? "" : obj.ToString();
        }

        public static bool IsNullOrZero<T>(this IEnumerable<T> list)
        {
            return list == null || list.GetEnumerator().Current == null;
        }

        public static bool IsNotNullAndZero<T>(this IEnumerable<T> list)
        {
            return list != null && list.GetEnumerator().Current != null;
        }

    }

 }
