﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using ShowBing.Core.Models;
using ShowBing.Entities;

namespace ShowBing.Core
{
    public class BingWallpaperHelper
    {
        private  const string BingSite ="https://cn.bing.com";
        
        public static List<BingImageMeta> GetLastBingImages(int lastDays = 7)
        { 
            var bingImageUrl=   "https://cn.bing.com/HPImageArchive.aspx?idx=0&n=" + lastDays;
            var xmlDoc=new XmlDocument();
            var list=new List<BingImageMeta>();
            try
            {
                var xml =  DownloadString(bingImageUrl,System.Text.Encoding.UTF8);
                xmlDoc.LoadXml(xml);
                XmlNodeList nodeList= xmlDoc.SelectNodes("/images/image");

                foreach (XmlNode node in nodeList)
                {
                    BingImageMeta bingImage=new BingImageMeta()
                    {
                        StartDate=node.SelectSingleNode("startdate").InnerText,
                        FullStartDate=node.SelectSingleNode("fullstartdate").InnerText,
                        EndDate=node.SelectSingleNode("enddate").InnerText,
                        Url=BingSite + node.SelectSingleNode("url").InnerText,
                        UrlBase=BingSite + node.SelectSingleNode("urlBase").InnerText,
                        Copyright=node.SelectSingleNode("copyright").InnerText,
                        CopyrightLink=node.SelectSingleNode("copyrightlink").InnerText,
                        Headline=node.SelectSingleNode("headline").InnerText,
                        Drk=node.SelectSingleNode("drk").InnerText,
                        Top=node.SelectSingleNode("top").InnerText,
                        Bot=node.SelectSingleNode("bot").InnerText,
                        HotSpots=node.SelectSingleNode("hotspots").InnerText
                    };
                    list.Add(bingImage);
                }
            }
            catch (Exception ex)
            {
                throw new ParseBingImageMetaException(ex);
            }

            return list; 
        }

        public static string DownloadString(string url, System.Text.Encoding encoding)
        {
            WebClient webClient = new WebClient();
            webClient.Encoding = encoding;
            return webClient.DownloadString(url);
        }

        public static void DownloadFile(string url,string fileName)
        {
            WebClient webClient=new WebClient();
            webClient.DownloadFile(url, fileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root">磁盘物理目录</param>
        /// <param name="bingImage"></param>
        public static void DownloadBingImageAndSave(BingImage bingImage, string root,Action<string> imageHandler)
        {
            var fileName = root.TrimEnd('/') +"/" + bingImage.Url.TrimStart('/');
            var dir= Path.GetDirectoryName(fileName);
            Directory.CreateDirectory(dir);

            if (File.Exists(fileName))
            {
                return;
            }

            DownloadFile(bingImage.BingUrl, fileName);
            imageHandler?.Invoke(fileName); 
        }

        /// <summary>
        /// 保存Bing图片对应的元数据信息
        /// </summary>
        /// <param name="bingImageMetas"></param>
        /// <param name="root"></param>
        public static void SaveMeta(IEnumerable<BingImageMeta> bingImageMetas,string root)
        {
            foreach (var item in bingImageMetas)
            {
                var fileName = root.TrimEnd('/') +"/bingmeta/" + item.EndDate.Substring(0,6)+"/"+item.EndDate+".txt";
                var dir = Path.GetDirectoryName(fileName);
                Directory.CreateDirectory(dir);

                if (File.Exists(fileName)) continue;

                System.Text.Json.JsonSerializerOptions options=new System.Text.Json.JsonSerializerOptions()
                {
                    WriteIndented=true, 
                };

                string json = System.Text.Json.JsonSerializer.Serialize(item,options);
                File.WriteAllText(fileName, json, System.Text.Encoding.UTF8);
            }
        }



        /// <summary>
        /// 获取缩略图文件名称
        /// </summary>
        /// <param name="imageFileName"></param>
        /// <returns></returns>
        public static string GetThumbnailFileName(string imageFileName)
        {
           return imageFileName.Insert(imageFileName.LastIndexOf("."),".th");
        }
    }
}
