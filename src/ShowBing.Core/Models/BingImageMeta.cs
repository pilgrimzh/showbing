﻿using System;

namespace ShowBing.Core.Models
{

    public class BingImageMeta
    {
        //<startdate>20201210</startdate>
        //<fullstartdate>202012100800</fullstartdate>
        //<enddate>20201211</enddate>
        //<url>/th?id=OHR.QueenoftheAndes_ZH-CN9019108680_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp</url>
        //<urlBase>/th?id=OHR.QueenoftheAndes_ZH-CN9019108680</urlBase>
        //<copyright>以布兰卡山脉为背景的安地斯皇后（莴氏普亚凤梨），秘鲁(© Cyril Ruoso/Minden Pictures)</copyright>
        //<copyrightlink>https://www.bing.com/search?q=%E5%B8%83%E5%85%B0%E5%8D%A1%E5%B1%B1%E8%84%89&form=hpcapt&mkt=zh-cn</copyrightlink>
        //<headline/>
        //<drk>1</drk>
        //<top>1</top>
        //<bot>1</bot>
        //<hotspots/>

        public string StartDate { get; set; }
        public string FullStartDate { get; set; }
        public string EndDate { get; set; }
        public string Url { get; set; }
        public string UrlBase { get; set; }
        public string Copyright { get; set; }
        public string CopyrightLink { get; set; }
        public string Headline { get; set; }
        public string Drk { get; set; }
        public string Top { get; set; }
        public string Bot { get; set; }
        public string HotSpots { get; set; }

        public string Date
        {
            get
            {
                if (DateTime.TryParseExact(EndDate, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AssumeLocal, out var dateTime))
                {
                    return dateTime.ToString("yyyy/MM/dd");
                }
                return EndDate;
            }
        } 
    }
}
