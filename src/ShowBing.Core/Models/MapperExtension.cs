﻿using System.Collections.Generic;
using ShowBing.Entities;
using Mapster;
using System;
using ShowBing.Core.Models;

namespace ShowBing.Core 
{
    public static class MapperExtension
    {
        //private static TypeAdapterConfig config=new TypeAdapterConfig(); 
        //static MapperExtension()
        //{
        //    config.ForType<BingImageMeta, BingImage>()
        //       .Map(d => d.CopyrightLink, s => s.CopyrightLink)
        //       .Map(d => d.Copyright, s => s.Copyright)
        //       .Map(d => d.BingUrl, s => s.Url)
        //       .Map(d => d.ImageDate, s => s.EndDate); 
        //}

        public static List<BingImage> Map(this IEnumerable<BingImageMeta> src)
        {
            List<BingImage> images=new List<BingImage>();
            foreach (var item in src)
            {
                var image= item.Adapt<BingImage>();
                var suffix=Guid.NewGuid().ToString("N").Substring(0,6);
                image.BingUrl = item.Url;
                image.ImageDate = Convert.ToInt32(item.EndDate);
                image.Url = $"/bing/{item.EndDate.Substring(0, 6)}/{item.EndDate}{suffix}.jpg";
                images.Add(image);
            }

            return images;
        }
    }
}
