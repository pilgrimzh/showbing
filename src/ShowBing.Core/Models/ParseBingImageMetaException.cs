﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowBing.Core.Models
{
    public class ParseBingImageMetaException:Exception
    {
        public ParseBingImageMetaException( ) : base("解析必应图像元数据出错！")
        {
        }
        public ParseBingImageMetaException(string message):base(message)
        {
        }
        public ParseBingImageMetaException(Exception ex) : base("解析必应图像元数据出错！" + ex.Message, ex.InnerException)
        {

        }

    }
}
