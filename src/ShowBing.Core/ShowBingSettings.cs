﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowBing.Core
{
    public class ShowBingSettings
    {
        public string Connection { get; set; }

        public ImageSettings Thumbnail { get; set; }
    }

    public class ImageSettings
    {
        public int Width { get; set; }
        public int Height { get; set; }

        /// <summary>
        /// 水印
        /// </summary>
        public bool WaterMark { get; set; }
        /// <summary>
        /// 水印文本
        /// </summary>
        public string WaterMarkText { get; set; }
        /// <summary>
        /// 左上角为：0，顺时针旋转 0(坐上),1(右上),2(右下),3(左下)
        /// </summary>
        public int Kind { get; set; } = 3;
        /// <summary>
        /// 文本透明度0-255
        /// </summary>
        public int Opacity { get; set; }
        /// <summary>
        /// 文本眼色
        /// </summary>
        public string Color { get; set; }
    }
}
