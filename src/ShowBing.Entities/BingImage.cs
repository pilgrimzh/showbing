﻿using System;
using FreeSql;
using FreeSql.DataAnnotations;
namespace ShowBing.Entities
{
    public class BingImage
    {

        [Column(Name ="id",IsPrimary =true,IsIdentity =true)]
        public long  Id { get; set; }

        /// <summary>
        /// 图片日期20201211
        /// </summary>
        [Column(Name ="image_date")]
        public int ImageDate { get; set; }

        [Column(Name = "bing_url",StringLength =1024)]
        public string BingUrl { get; set; }

        [Column(Name = "width")]
        public int Width { get; set; } = 1920;
        [Column(Name = "height")]
        public int Height { get; set; } = 1080;

        /// <summary>
        /// 文件位置 /yyyyMM/yyyyMMdd_1920x1080.jpg
        /// </summary>
        [Column(Name = "url",StringLength =255)]
        public string Url { get; set; }

        [Column(Name = "copyright", StringLength =255)]
        public string Copyright { get; set; }

        [Column(Name = "copyright_link", StringLength = 1024)]
        public string CopyrightLink { get; set; }

    }

}
