﻿using System;

namespace ShowBing.Repositories
{
    public abstract class AbsBaseRepository<TEnity> :FreeSql.BaseRepository<TEnity, long> where TEnity:class
    {
        public AbsBaseRepository(IFreeSql freeSql):base(freeSql,null,null)
        {
        }
    }



}
