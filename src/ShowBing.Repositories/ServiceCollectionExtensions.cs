﻿using System;
using System.Reflection;
using FreeSql;
using Microsoft.Extensions.DependencyInjection;

namespace ShowBing.Repositories
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="services"></param>
        public static void InjectRepositories(this IServiceCollection services)
        {
            var types = Assembly.Load("ShowBing.Repositories").GetTypes();
            var baseRepo = typeof(IBaseRepository);
            foreach (Type type in types)
            {
                if (baseRepo.IsAssignableFrom(type) && type.IsAbstract == false&&type.IsInterface==false)
                {
                    services.AddTransient(type);
                }
            } 
        }


    }
}
