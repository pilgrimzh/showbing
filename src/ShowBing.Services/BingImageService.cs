﻿using ShowBing.Repositories;
using System.Collections.Generic;
using ShowBing.Entities;
using ShowBing.Core.Dto;
using ShowBing.Core.Dto.Response;

namespace ShowBing.Services
{
    public class BingImageService: AbsBaseService
    {
        private readonly BingImageRepository _repo;

        public BingImageService(BingImageRepository bingImageRepository)
        {
            _repo = bingImageRepository;
        }

        /// <summary>
        /// 指定日期的图片 是否存在
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool Exist(int date)
        {
            return _repo.Select.Any(x => x.ImageDate == date);
        }

        public void Insert(IEnumerable<BingImage> images)
        {
            _repo.Insert(images);
        }

        public PageResult<BingImageOutput> GetPage(BingImagePageInput input)
        {
          var list=   _repo.Select.WhereIf(int.TryParse(input.Begin, out var begin), x => x.ImageDate >= begin)
                .WhereIf(int.TryParse(input.End, out var end), x => x.ImageDate <= end)
                .Count(out long total)
                .OrderByDescending(x => x.ImageDate)
                .Page(input.Page, input.Limit)
                .ToList<BingImageOutput>();

            foreach (var item in list)
            {

            }

            return ResultHelper.SuccessPageResult(list, total);
        }

        /// <summary>
        /// 返回最新的几个图片所在的日期
        /// </summary>
        /// <param name="topCount"></param>
        /// <returns></returns>
        public IEnumerable<int> GetLastestBingImages(int topCount=7)
        {
            return _repo.Select.OrderByDescending(x => x.ImageDate).Page(1, topCount).ToList(x => x.ImageDate);
        }


    }


}
