﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using ShowBing.Repositories;
using ShowBing.Core;

namespace ShowBing.Services
{
    public static class ServiceCollectionExtensions
    {
         

        /// <summary>
        /// 注入redis,freesql,repositories,services服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="redisConnect"></param>
        public static void InjectServices(this IServiceCollection services,ShowBingSettings showBingSettings)
        { 
            #region freesql 
            IFreeSql freeSql = new FreeSql.FreeSqlBuilder()
                .UseAutoSyncStructure(true)
                .UseConnectionString(FreeSql.DataType.Sqlite,showBingSettings.Connection)
                .Build();

            freeSql.Aop.CommandBefore += (sender,e)=> { };
            freeSql.Aop.CommandAfter += (sender, e) =>
            {
                if(e.ElapsedMilliseconds > 100)
                {
                    Console.WriteLine($"--------------------------------------------------------------------------------");
                    Console.WriteLine($"{Environment.NewLine}执行Sql({e.ElapsedMilliseconds}ms > 100ms) : { e.Command.CommandText}");
                    Console.WriteLine($"--------------------------------------------------------------------------------");
                }
            };
             
            services.AddSingleton(freeSql);
            #endregion

            #region Repositories
             
            services.InjectRepositories();
            #endregion

            #region Services 

            var types = Assembly.Load("ShowBing.Services").GetTypes();
            var appServiceType = typeof(AbsBaseService);
            foreach (Type type in types)
            {
                if (appServiceType.IsAssignableFrom(type) && type.IsAbstract == false )
                {
                    services.AddTransient(type);
                }
            }
              

            #endregion

        }

        
    }
}
