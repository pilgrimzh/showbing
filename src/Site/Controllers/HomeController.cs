﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using showbing.Models;
using ShowBing.Services;
using ShowBing.Core.Dto;

namespace showbing.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly BingImageService _bingImageService;

        public HomeController(ILogger<HomeController> logger,BingImageService bingImageService)
        {
            _logger = logger;
            _bingImageService = bingImageService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取分页内容
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Page(ShowBing.Core.Dto.Response.BingImagePageInput input)
        {
            var result= _bingImageService.GetPage(input);
            return Json(result);
        }

         

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
