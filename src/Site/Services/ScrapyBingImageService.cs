﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using ShowBing.Services;
using ShowBing.Entities;
using ShowBing.Core;
using Hangfire;
using System.Drawing;
using System.IO;

namespace showbing.Services
{
    public class ScrapyBingImageService : IHostedService
    {
        private static readonly NLog.ILogger logger=NLog.LogManager.GetCurrentClassLogger();

        private readonly BingImageService _bingImageService;
        private readonly ShowBingSettings _showBingSettings;

        public ScrapyBingImageService(BingImageService bingImageService, ShowBingSettings showBingSettings)
        {
            _bingImageService = bingImageService;
            _showBingSettings = showBingSettings;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            BackgroundJob.Enqueue(() => DoJob());
            RecurringJob.AddOrUpdate(() => DoJob(), Cron.Daily(0, 1));

            return Task.CompletedTask;
        }


        public void DoJob()
        {
            logger.Info("开始执行Bing采集处理操作...");
            try
            {
                 
                var bingImageMetas= BingWallpaperHelper.GetLastBingImages();
                logger.Info("采集Bing图片信息完成...");

                var dates= _bingImageService.GetLastestBingImages();
                bingImageMetas.RemoveAll(x => dates.Any(d => d == Convert.ToInt32(x.EndDate))); 
                //存储Meta数据
                var images= bingImageMetas.Map();
                var root = "wwwroot";
                foreach (var item in images)
                {
                    BingWallpaperHelper.DownloadBingImageAndSave(item, root, CreateThumbnail);
                }
                logger.Info("存储Bing图片文件完成...");

                BingWallpaperHelper.SaveMeta(bingImageMetas, root);
                logger.Info("存储Bing图片元数据文件完成...");

                _bingImageService.Insert(images);
                logger.Info("Bing图片信息入库完成，采集结束！");

            }
            catch (Exception ex)
            {
                logger.Error(ex, "采集、处理图片过程中出错！" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 图片下载完成后，生成缩略图
        /// </summary>
        /// <param name="imageFile"></param>
        private void CreateThumbnail(string imageFile)
        {
            var thumbnailFileName= BingWallpaperHelper.GetThumbnailFileName(imageFile);
            logger.Info($"create thumbnail file:{thumbnailFileName}");

            using (Bitmap bitmap = new Bitmap(_showBingSettings.Thumbnail.Width, _showBingSettings.Thumbnail.Height))
            using (var g = Graphics.FromImage(bitmap))
            using (System.Drawing.Image srcImage = Image.FromFile(imageFile))
            {
                g.Clear(Color.White);
                var format=srcImage.RawFormat;
                g.DrawImage(srcImage, 0, 0, bitmap.Width, bitmap.Height);
                bitmap.Save(thumbnailFileName, format);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
