using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Hangfire;
using Hangfire.MemoryStorage;
using ShowBing.Services;
using showbing.Services;
using ShowBing.Core;

namespace showbing
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ShowBingSettings showBingSettings=new ShowBingSettings();
            Configuration.GetSection(nameof(ShowBingSettings)).Bind(showBingSettings);
            services.AddSingleton(showBingSettings);

            // Add Hangfire services.
            services.AddHangfire(config=>config.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                        .UseSimpleAssemblyNameTypeSerializer()
                        .UseRecommendedSerializerSettings()
                        .UseMemoryStorage()) ;
            services.InjectServices(showBingSettings);
            services.AddHostedService<ScrapyBingImageService>();
            // Add the processing server as IHostedService
            services.AddHangfireServer();
            services.AddControllersWithViews(option=> { 
             
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //��Դ��ַ
            string[] resource=new string[]{"https://cdn.bootcdn.net" };

            app.UseCsp(options => options
                              .DefaultSources(s => s.Self())
                              .ObjectSources(x => x.Self())
                              .FontSources(x => x.Self().CustomSources("data:").CustomSources(resource))
                              .StyleSources(x => x.Self().UnsafeInline().CustomSources(resource))
                             //.FrameAncestors(x => x.Self())
                              .ImageSources(img => img.Self().CustomSources(resource).CustomSources("data:"))
                              .MediaSources(x => x.Self().CustomSources(resource))
                             //.FrameSources(s => s.Self().CustomSources("192.168.1.129"))
                              .ConnectSources(s => s.Self())
                              .ScriptSources(s => s.Self().CustomSources(resource)
                                              .UnsafeEval()
                                              .UnsafeInline())
                              //.ReportUris(x => x.Uris("/Csp/Report"))
              );


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            //Registered before static files to always set header 
            //https://www.cnblogs.com/zhangzhijian/p/10579801.html 
            //X-Content-Type-Options: nosniff
            app.UseXContentTypeOptions();
            app.UseReferrerPolicy(opts => opts.NoReferrer());
            app.UseXXssProtection(options => options.EnabledWithBlockMode());
            app.UseXfo(xfo => xfo.SameOrigin());

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseHangfireDashboard();
             
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapHangfireDashboard();
            });
        }
    }
}
